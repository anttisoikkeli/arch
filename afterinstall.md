### Cleanup

#### Check and remove orphans
```
pacman -Qtdq
sudo pacman -Rns $(pacman -Qtdq)
```
#### Sizes of explicitly installed packages not in base, base-devel, gnome or gnome-extra group ("Programs")
```
expac -H M "%011m\t%-20n\t%10d" $(comm -23 <(pacman -Qqe | sort) <(pacman -Qqg base base-devel gnome gnome-extra | sort)) | sort -nr
```
#### Explicitly installed packages inside gnome or gnome-extra group not needed as dependencies ("Operating System"):
```
pacman -Qqget gnome gnome-extra
```
#### Sizes of all packages
```
pacman -Qi | awk '/^Name/{name=$3} /^Installed Size/{print $4$5, name}' | sort -h
```
#### External packages:
```
pacman -Qme
```
#### Enable CR2 thumbnails
```
yay -S ufraw-thumbnailer
```
#### Get pictures from camera
```
sudo pacman -S gphoto2
gphoto2 --auto-detect
gphoto2 --get-all-files
```

#### Python
Pyenv:
```
git clone https://github.com/pyenv/pyenv.git ~/.pyenv
echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bash_profile
echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bash_profile
```


#### Enable U2F Keys
* Copy 70-u2f.rules from dotfiles to /etc/udev/rules.d/ (or original here: https://github.com/Yubico/libu2f-host/blob/master/70-u2f.rules)
* Go in Firefox about:config, search for u2f and double click to enable
* Reboot

#### ykman-gui
```
sudo pacman -S qt5-quickcontrols2 qt5-graphicaleffects
git clone https://github.com/Yubico/yubikey-manager-qt.git
cd yubikey-manager-qt
```
#### Problems connecting to yubikey:
```bash
sudo systemctl start pcscd
sudo systemctl start pcscd.socket
```
#### Yubikey reset openpgp:
* More info: https://developers.yubico.com/PGP/Card_edit.html
```bash
ykman openpgp reset
ykman openpgp set-pin-retries 10 10 10
ykman openpgp set-touch sig on
ykman openpgp set-touch enc on
ykman openpgp set-touch aut on
gpg --card-edit
admin
passwd
1
passwd
3
q
```
* More about importing stuff: https://www.linode.com/docs/security/authentication/gpg-key-for-ssh-authentication/

### Customizations
```
Text color: #1793D1
Background: #2A2A2A
Text color: #00FF00
Background: #303030
```
### Make your scripts programs ("Install")
#### Make executable as command:
Just copy to /usr/bin
#### Add to program list
Add this (minimum) to /usr/share/applications/<scriptname>.desktop:
```
[Desktop Entry]
Type=Application
Name=<name>
Exec=/usr/bin/<name> 
```

### TrueCrypt
Create a new container foo.tc, 20M in size for instance, in the working directory:
```
fallocate -l 20M foo.tc
losetup /dev/loop0 foo.tc
tcplay -c -d /dev/loop0 -a whirlpool -b AES-256-XTS
```
Enter a secure password for the volume, and confirm the query to overwrite foo.tc with the new volume. tcplay will then write random data into the volume. Map the volume and create a filesystem on it in order to mount
```
tcplay -m foo.tc -d /dev/loop0
mkfs.ext4 /dev/mapper/foo.tc
mount /dev/mapper/foo.tc /mnt/truecrypt/
```
To unset the container,
```
umount /mnt/truecrypt
dmsetup remove foo.tc
losetup -d /dev/loop0
```
### Firefox
* Enable tracking protection
* Disable ask to save logins (+clear saved logins)
* Firefox will: Never remember history (+clear history)
* Tracking protection: Always
* Send websites "Do Not Track": Always
* Install Ublock Origin

### Java
```
sudo pacman -S jre10-openjdk
```
Run with command:
```
java -jar <jar file>
```
### Dash to dock:
May cause crashes. In the past it helped to choose "Show the dock on: Primary monitor" and "Show on all monitors."
```
yay -S gnome-shell-extension-dash-to-dock
```
### Install yay or aurman
```
mkdir builds 
cd builds

git clone https://aur.archlinux.org/yay-bin.git
or
cd yay-bin
makepkg -cis

git clone https://aur.archlinux.org/aurman.git
cd aurman
gpg --recv-keys 465022E743D71E39
makepkg -cis
```
### Wallpapers:
http://oswallpapers.com/
### Theming:
```
yay -S gnome-osc-hs-gtk-theme
yay -S macos-icon-theme
yay -S capitaine-cursors
```
Tweaks->Appearance->Themes->Applications+Cursor+Icons

### Western digital green:
Please notice that time is x5s (12=60s):
```
sudo pacman -S idle3-tools
sudo idle3ctl -s 12 /dev/sdX
```

### If filezilla slow (https://forum.filezilla-project.org/viewtopic.php?t=40513)
```
sudo sysctl -w net.ipv4.tcp_rmem='40960 873800 62914560'
sudo sysctl -w net.core.rmem_max=8388608

sudo touch /etc/sysctl.d/99-sysctl.conf
sudo nano /etc/sysctl.d/99-sysctl.conf
net.ipv4.tcp_rmem=40960 873800 62914560
net.core.rmem_max=8388608
```


### Fonts
```
yay -S otf-san-francisco adobe-source-code-pro-fonts
```
Tweaks-Fonts
* Window title Cantarelli Bold 11 -> SF Pro Display Bold
* Interface Cantarell Regular 11 -> SF Pro Display Regular
* Document Sans Regular 11 -> SF Pro Text Regular
* Monospace Monospace Regular -> Source COde Pro Regular

For terminal:
```
system-san-francisco-font-git
```

### Install powerpill
```
yay -S powerpill
```
Also edit /etc/pacman.conf libs look like this or you'll get errors with powerpill:
```
[core]
SigLevel = PackageRequired
Include = /etc/pacman.d/mirrorlist
```
### Create update script and automate it
Generate "update" -file to your homefolder
```
#!/bin/bash
#reflector --verbose --country Austria --country Belarus --country Belgium --country "Bosnia and Herzegovina" --country Bulgaria --country Croatia --country Czechia --country Denmark --country Finland --country France --country Germany --country Greece --country Hungary --country Iceland --country Ireland --country Israel --country Italy --country Latvia --country Lithuania --country Luxembourg --country Netherlands --country Norway --country Poland --country Portugal --country Romania --country Serbia --country Slovakia --country Slovenia --country Spain --country Sweden --country Switzerland --country Ukraine --country "United Kingdom" --latest 25 --sort rate --save /etc/pacman.d/mirrorlist
curl -o /etc/pacman.d/mirrorlist 'https://www.archlinux.org/mirrorlist/?country=AT&country=BY&country=BE&country=BA&country=BG&country=HR&country=CZ&country=DK&country=FI&country=FR&country=DE&country=GR&country=HU&country=IS&country=IE&country=IL&country=IT&country=LV&country=LT&country=LU&country=NL&country=NO&country=PL&country=PT&country=RO&country=RS&country=SK&country=SI&country=ES&country=SE&country=CH&country=UA&country=GB&protocol=http&protocol=https&ip_version=4&ip_version=6&use_mirror_status=on'
sed -i 's/^#Server/Server/' /etc/pacman.d/mirrorlist
powerpill -Syu
```
Then just run this command from your update button
```
gnome-terminal -- sudo ~/update
```
### Install corsair drivers:
```
yay -S ckb-next
sudo systemctl start ckb-next-daemon
sudo systemctl enable ckb-next-daemon
```
### Install visual studio code
```
yay -S code
```
### Install virtual box
Install (choose arch for normal and dkms for hardened)
```
sudo pacman -S virtualbox
```
If hardened, also
```
sudo pacman -S linux-hardened-headers
```
Start manually here or reboot
```
sudo modprobe -a vboxdrv
```
add modules for loading on startup
```
echo 'vboxdrv' | sudo tee --append /etc/modules-load.d/virtualbox.conf
```
add user to vboxusers -group
```
sudo gpasswd -a $USER vboxusers
```
Install extension pack for usb2 support:
```
yay -S virtualbox-ext-oracle
```
#### Inside guest for shared folders:
Spice guest tools:
https://www.spice-space.org/download.html
Shared folders:
https://www.spice-space.org/download/windows/spice-webdavd/

Install guest utils:
```
sudo pacman -S virtualbox-guest-utils

# Don't install nox, it doesn't have clipboard support
# sudo pacman -S virtualbox-guest-utils-nox

```

To make sure you have right modules (arch if normal, dkms if hardened):
```
sudo pacman -S virtualbox-guest-modules-arch
sudo pacman -S virtualbox-guest-dkms
```
Add user to vboxsf group to enable shared folders and give access to /media:
```
sudo gpasswd -a $USER vboxsf
sudo chmod 755 /media
```
```
sudo systemctl enable vboxservice.service
```
### Fix hard drives
```
sudo mount /dev/sdb1 ~/SSD
```

### Mount drives under /mnt and add to fstab (/etc/fstab)
```
 <device>	<dir>		<type>    <options>             <dump> <fsck>
/dev/sdb1	/mnt/SSD	ext4      defaults,noatime      0      2
/dev/sdc1	/mnt/HDD	ext4      defaults,noatime      0      2
```

### Install Elm:
https://github.com/sindresorhus/guides/blob/master/npm-global-without-sudo.md
###### 1. Create a directory for global packages
```sh
mkdir "${HOME}/.npm-packages"
```
###### 2. Indicate to `npm` where to store globally installed packages. In your `~/.npmrc` file add:
```sh
prefix=${HOME}/.npm-packages
```
###### 3. Ensure `npm` will find installed binaries and man pages. Add the following to your `.bashrc`/`.zshrc`:

```sh
NPM_PACKAGES="${HOME}/.npm-packages"

PATH="$NPM_PACKAGES/bin:$PATH"

# Unset manpath so we can inherit from /etc/manpath via the `manpath` command
unset MANPATH # delete if you already modified MANPATH elsewhere in your config
export MANPATH="$NPM_PACKAGES/share/man:$(manpath)"
```
Then just
```
npm install -g elm
```
You may start with (needs relogin):
```
elm-package install
```

### Install this if pgadmin4 doesn't work:

https://extensions.gnome.org/extension/1031/topicons/


### Install rust

#### Prefer original
* https://rustup.rs/


#### Through package management
```
sudo pacman -S rustup
rustup install stable
rustup default stable-x86_64-unknown-linux-gnu
```

### Install some rust compability
#### Don't remember if needed anymore
```
rustup component add rust-src
```
#### For text editor support
```
sudo pacman -S rust-racer
```
add rust source code to make Racer autocompletion work?
```
gedit plugin
```
https://github.com/isamert/gracer
```
mkdir -p ~/.local/share/gedit/plugins
cd ~/.local/share/gedit/plugins
git clone https://github.com/isamert/gracer.git
```
Open gedit, go to Preferences > Plugins > Gracer to activate it.
From plugins menu, open Gracer's Preferences.
Set Racer path and Rust source path.
Restart gedit.
update:
```
cd ~/.local/share/gedit/plugins/gracer
git pull
```
##### Remember to add rust source code if needed rust

### Install Monero
```
aurman -S monero
```

## Create Monero node:
```
wget https://downloads.getmonero.org/cli/linux64
tar xf linux64
To .bash_profile: ~/monero-v0.14.0.2/monerod
```

### Install dmg2img
```
yay -S dmg2img
```

### Install mellowplayer
```
pacaur -S mellowplayer
sudo pacman -S pepper-flash
```

### Install file sharing
ReadyMedia (minidlna) 
* Remember that the hard disc needs to be mounted with fstab for permission
* Edit /etc/minidlna.conf (right folder and user to root)
* Edit /usr/lib/systemd/system/minidlna.service (user to root)
```
systemctl enable minidlna.service
systemctl start minidlna.service
killall minidlnad
```

#### In case needed:
```
rebuild:
minidlnad -R
```


### To install crossover (wine ++), please check your harddrive
##### enable -> /etc/pacman.conf 
```
[multilib]
Include = /etc/pacman.d/mirrorlist
```
And:
```
aurman -S crossover
```


## Gnome specific
Change languages and local formats
Code example: gsettings set org.gnome.desktop.input-sources sources "[('xkb', 'fi')]"
Code example: gsettings set org.gnome.desktop.background picture-uri "'file:///home/users/Pictures/background.jpg'"
Tweak windows: turn on minimize, maximize
Tweak extensions user themes on, Launch new instance
```
aurman -S chrome-gnome-shell-git gnome-shell-extension-no-topleft-hot-corner
```
https://extensions.gnome.org/extension/358/activities-configurator/

Install plugin
```
## Deprecated: pacaur -S gnome-shell-extension-activities-config
```
Activities configurat on -> Hide text, Disable Hot corner, Panel transparency 70
```
aurman -S gnome-shell-extension-dash-to-dock
```
Dash to dock on -> Shrink dash, show counter indications, customize opacity 40%


## Garbage bin
### Nvidia:
```
sudo pacman -S lib32-nvidia-utils ttf-liberation steam lib32-primus primus libvdpau
```
#### Also to remember:
primusrun %command%
removed primus lib32-primus lib32-nvidia-utils steam
installed nvidia and removed all nvidia
