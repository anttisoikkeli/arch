#!/usr/bin/bash

#alias hask='bash ~/haskcompile.sh'

FILE="$(pwd)/$1.hs"
OFILE="$(pwd)/$1.o"
HIFILE="$(pwd)/$1.hi"
EXEFILE="$(pwd)/$1"

if [ $# -eq 0 ]; then
    echo "Error: No arguments supplied."
    exit 1
else
  stack ghc -- $FILE
  strip $EXEFILE
  rm $OFILE $HIFILE
fi