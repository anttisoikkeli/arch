#!/usr/bin/bash

#alias hask='bash ~/haskscript.sh'

FILE="$(pwd)/$1.hs"

function generate_script() {
  local resolver=$1
cat <<DOC
#!/usr/bin/env stack
-- stack --resolver ${resolver} script
main :: IO ()
main = putStrLn "Hello, world!"
DOC
}

if [ $# -eq 0 ]; then
    echo "Error: No arguments supplied."
elif [ -f "$FILE" ]; then
  echo "Error: ${FILE} found. Can not continue."
  exit 1
else
  RESOLVER="$(stack ls snapshots --lts remote | grep -o 'lts.*' | tail -1)"
  generate_script ${RESOLVER} > ${FILE}
  chmod +x ${FILE}
  vim ${FILE} </dev/null &>/dev/null &
fi