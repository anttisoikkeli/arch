# Light3rn Arch install script
Arch install script

1) Boot Installation
2) ```loadkeys xx```
3) ```fdisk -l```
4) If UEFI:
* First open  with ```fdisk /dev/sdX```
* Then first let's create new GPT partition table and EFI partition:
* ```g``` -> ```n ``` -> ```(default)``` -> ```(default)``` -> ```+512M ``` -> ```yes``` -> ```t ``` -> ```1 ```
* Then let's use what's left for main partition (you may do what you like):
* ```n ``` -> ```default ``` -> ```default ``` -> ```default ```-> ```yes``` -> ```w``` 
5) If Non-UEFI, partition with ```fdisk: /dev/sdX``` and ```o``` -> ```n ``` -> ```ok to everything``` -> ```a``` -> ```w```
7) ```wget https://gitlab.com/lightern/arch/-/archive/master/arch-master.tar.gz -O - | tar xz``` (or separately when using tar -xvf)
8) ```cd arch-master```
9) Edit installed programs in archwayland or archx: ```nano archwayland```
10) ```chmod +x install```
11) ```./install```
12) Follow instructions
